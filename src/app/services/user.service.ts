import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly userURL: string;

  constructor(private http: HttpClient) {
    this.userURL = 'http://localhost:8080/user/all';

  }

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.userURL);
  }
}
