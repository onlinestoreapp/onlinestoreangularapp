import { NgModule } from '@angular/core';
import {UserListComponent} from './user-list/user-list.component';
import {Routes, RouterModule} from '@angular/router';


const routes: Routes = [
  {
    path: 'users',
    component: UserListComponent

  }];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
